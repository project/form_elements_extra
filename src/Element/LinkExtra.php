<?php

namespace Drupal\form_elements_extra\Element;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\link\LinkItemInterface;

/**
 * Provides a link form element.
 *
 * For the moment there is no 'link' form element defined in core.
 * Instead the Link widget "hardcode" how it constructs its form element.
 * This means every time you need a form element for a link, you still have to
 * implement either an 'url' or an 'entity_autocomplete' element with title,
 * and other attributes needed.
 *
 * This is kind of heavy so we decide, until this is fixed,
 * to had a custom 'link_extra' form element that implements it.
 *
 * An issue has been created: {@link https://www.drupal.org/node/2866997}.
 *
 * The default behavior of the 'link_extra' form element is to support
 * both internal and external URLs with a target attribute.
 *
 * If this idea is added to core one day then the probability that the
 * target attribute is removed is very high.
 *
 * Usage example:
 * @code
 * $form['link'] = [
 *   '#type' => 'link_extra',
 *   '#title' => t('Link Extra'),
 *   '#default_value' => [
 *     'uri' => 'http://example.com',
 *     'title' => 'Foo',
 *     'attributes' => [
 *       'target' => '_blank',
 *     ],
 *   ],
 *   '#uri_options' => [
 *     '#required' => TRUE,
 *   ],
 *   '#title_options' => [
 *     '#required' => TRUE,
 *   ],
 *   '#attributes_options' => [
 *     '#open' => TRUE,
 *     'target' => [
 *       '#title' => t('Custom title'),
 *     ],
 *   ],
 * ];
 * @endcode
 *
 * @FormElement("link_extra")
 */
class LinkExtra extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      // By default the form element supports both internal and external URLs.
      '#support' => LinkItemInterface::LINK_GENERIC,
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#default_value' => NULL,
      '#process' => [
        [$class, 'processLinkUri'],
        [$class, 'processLinkTitle'],
        [$class, 'processLinkAttributes'],
        [$class, 'processForceTree'],
      ],
      '#theme_wrappers' => ['fieldset'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if (is_array($input)) {
      return $input;
    }

    if (!is_array($element['#default_value'])) {
      $element['#default_value'] = [];
    }

    // Initialize properties.
    $properties = ['uri', 'title', 'attributes'];
    foreach ($properties as $property) {
      if (!isset($element['#default_value'][$property])) {
        $element['#default_value'][$property] = NULL;
      }
    }

    return $element['#default_value'];
  }

  /**
   * Force to use hierarchical values.
   *
   * The values of this element and its children should be
   * hierarchical in $form_state.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @see https://www.drupal.org/project/drupal/issues/759222
   */
  public static function processForceTree(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element['#tree'] = TRUE;
    return $element;
  }

  /**
   * Processes the link's URI form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processLinkUri(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (isset($element['#support']) && !in_array($element['#support'], static::getAvailableSupports())) {
      throw new \InvalidArgumentException('The #support property is not valid.');
    }

    // Retrieves options.
    $uri_options = isset($element['#uri_options']) ? $element['#uri_options'] : [];

    // If the element does not support internal links,
    // the default form element we'll be 'url'.
    $element['uri'] = [
      '#type' => 'url',
      '#title' => new TranslatableMarkup('URL'),
      '#default_value' => NULL,
      '#element_validate' => [[get_called_class(), 'validateUriElement']],
      '#maxlength' => 2048,
    ];

    // If the element is configured to support internal links, it cannot use the
    // 'url' form element and we have to do the validation ourselves.
    if (in_array($element['#support'], [LinkItemInterface::LINK_INTERNAL, LinkItemInterface::LINK_GENERIC])) {
      $element['uri']['#type'] = 'entity_autocomplete';
      // @TODO The user should be able to select an entity type. Will be fixed
      // in https://www.drupal.org/node/2423093.
      $element['uri']['#target_type'] = 'node';
      // Disable autocompletion when the first character is '/', '#' or '?'.
      $element['uri']['#attributes']['data-autocomplete-first-character-blacklist'] = '/#?';

      // We are doing our own processing
      // in the method getUriAsDisplayableString.
      $element['uri']['#process_default_value'] = FALSE;
    }

    // If the field is configured to allow only internal links, add a useful
    // element prefix and description.
    if (LinkItemInterface::LINK_INTERNAL === $element['#support']) {
      $element['uri']['#field_prefix'] = rtrim(Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString(), '/');
      $element['uri']['#description'] = new TranslatableMarkup('This must be an internal path such as %add-node. You can also start typing the title of a piece of content to select it. Enter %front to link to the front page.', ['%add-node' => '/node/add', '%front' => '<front>']);
    }
    // If the field is configured to allow both internal and external links,
    // show a useful description.
    elseif (LinkItemInterface::LINK_GENERIC === $element['#support']) {
      $element['uri']['#description'] = new TranslatableMarkup('Start typing the title of a piece of content to select it. You can also enter an internal path such as %add-node or an external URL such as %url. Enter %front to link to the front page.', ['%front' => '<front>', '%add-node' => '/node/add', '%url' => 'http://example.com']);
    }
    // If the field is configured to allow only external links, show a useful
    // description.
    elseif (LinkItemInterface::LINK_EXTERNAL === $element['#support']) {
      $element['uri']['#description'] = new TranslatableMarkup('This must be an external URL such as %url.', ['%url' => 'http://example.com']);
    }

    // Override uri's options.
    $element['uri'] = array_replace_recursive($element['uri'], $uri_options);

    $uri_isset = (isset($element['#default_value']['uri']) && !empty($element['#default_value']['uri']));
    // The current value could have been entered by a different user.
    // However, if it is inaccessible to the current user, do not display it
    // to them.
    $user_has_access = static::userHasAccessToUri($element);
    // Set uri's default value.
    $element['uri']['#default_value'] = ($uri_isset && $user_has_access) ? static::getUriAsDisplayableString($element['#default_value']['uri']) : NULL;

    return $element;
  }

  /**
   * Processes the link's title form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processLinkTitle(array &$element, FormStateInterface $form_state, array &$complete_form) {
    // Retrieves options for different components of the element.
    $title_options = isset($element['#title_options']) ? $element['#title_options'] : [];

    // Define title's default options.
    $element['title'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Link text'),
      '#placeholder' => NULL,
      '#default_value' => NULL,
      '#maxlength' => 255,
      '#access' => TRUE,
      '#required' => FALSE,
    ];

    // Override title's options.
    $element['title'] = array_replace_recursive($element['title'], $title_options);

    if (isset($element['#default_value']['title']) && !empty($element['#default_value']['title'])) {
      // Set title's default value.
      $element['title']['#default_value'] = $element['#default_value']['title'];
    }

    return $element;
  }

  /**
   * Processes the link's attributes form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processLinkAttributes(array &$element, FormStateInterface $form_state, array &$complete_form) {
    // Retrieves options for different components of the element.
    $attributes_options = isset($element['#attributes_options']) ? $element['#attributes_options'] : [];

    // Define attributes' default options.
    $element['attributes'] = [
      '#type' => 'details',
      '#title' => new TranslatableMarkup('Attributes'),
      '#tree' => TRUE,
    ];

    // By default we add a target attribute.
    $element['attributes']['target'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Target'),
      '#description' => new TranslatableMarkup('Set the target attribute.'),
      '#empty_option' => new TranslatableMarkup('- Select -'),
      '#options' => [
        '_blank' => new TranslatableMarkup('New window (_blank)'),
        '_self' => new TranslatableMarkup('Same window (_self)'),
      ],
      '#default_value' => NULL,
    ];

    // Override attributes' options.
    $element['attributes'] = array_replace_recursive($element['attributes'], $attributes_options);

    if (isset($element['#default_value']['attributes']['target']) && !empty($element['#default_value']['attributes']['target'])) {
      // Set target attribute's default value.
      $element['attributes']['target']['#default_value'] = $element['#default_value']['attributes']['target'];
    }

    return $element;
  }

  /**
   * Gets the URI without the 'internal:' or 'entity:' scheme.
   *
   * The following two forms of URIs are transformed:
   * - 'entity:' URIs: to entity autocomplete ("label (entity id)") strings;
   * - 'internal:' URIs: the scheme is stripped.
   *
   * This method is the inverse of ::getUserEnteredStringAsUri().
   *
   * This method is almost the verbatim copy of
   * Drupal\link\Plugin\Field\FieldWidget\LinkWidget::getUriAsDisplayableString.
   *
   * @param string $uri
   *   The URI to get the displayable string for.
   *
   * @return string
   *   The URI without the 'internal:' or 'entity:' scheme.
   *
   * @see static::getUserEnteredStringAsUri()
   */
  protected static function getUriAsDisplayableString($uri) {
    $scheme = parse_url($uri, PHP_URL_SCHEME);

    // By default, the displayable string is the URI.
    $displayable_string = $uri;

    // A different displayable string may be chosen in case of the 'internal:'
    // or 'entity:' built-in schemes.
    if ($scheme === 'internal') {
      $uri_reference = explode(':', $uri, 2)[1];

      // @TODO '<front>' is valid input for BC reasons, may be removed by
      // https://www.drupal.org/node/2421941
      $path = parse_url($uri, PHP_URL_PATH);
      if ($path === '/') {
        $uri_reference = '<front>' . substr($uri_reference, 1);
      }

      $displayable_string = $uri_reference;
    }
    elseif ($scheme === 'entity') {
      // @TODO: Even if it's come from the core, hardcoded 7 and 2 need docs.
      list($entity_type, $entity_id) = explode('/', substr($uri, 7), 2);
      // Show the 'entity:' URI as the entity autocomplete would.
      // @todo Support entity types other than 'node'. Will be fixed in
      //    https://www.drupal.org/node/2423093.
      // At the moment we took that from core, it only support
      // node type. Be careful with updates.
      if ($entity_type == 'node' && $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id)) {
        $displayable_string = EntityAutocomplete::getEntityLabels([$entity]);
      }
    }

    return $displayable_string;
  }

  /**
   * Gets the user-entered string as a URI.
   *
   * The following two forms of input are mapped to URIs:
   * - entity autocomplete ("label (entity id)") strings: to 'entity:' URIs;
   * - strings without a detectable scheme: to 'internal:' URIs.
   *
   * This method is the inverse of ::getUriAsDisplayableString().
   *
   * This method is almost the verbatim copy of
   * Drupal\link\Plugin\Field\FieldWidget\LinkWidget::getUserEnteredStringAsUri.
   *
   * @param string $string
   *   The user-entered string.
   *
   * @return string
   *   The URI, if a non-empty $uri was passed.
   *
   * @see static::getUriAsDisplayableString()
   */
  protected static function getUserEnteredStringAsUri($string) {
    // By default, assume the entered string is an URI.
    $uri = trim($string);

    // Detect entity autocomplete string, map to 'entity:' URI.
    $entity_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($string);
    if ($entity_id !== NULL) {
      // @TODO Support entity types other than 'node'. Will be fixed in
      // https://www.drupal.org/node/2423093.
      $uri = 'entity:node/' . $entity_id;
    }
    // Detect a schemeless string, map to 'internal:' URI.
    elseif (!empty($string) && parse_url($string, PHP_URL_SCHEME) === NULL) {
      // @TODO '<front>' is valid input for BC reasons, may be removed by
      // https://www.drupal.org/node/2421941
      // - replace '<front>' with '/'
      // - replace '<front>#foo' with '/#foo'
      if (strpos($string, '<front>') === 0) {
        $string = '/' . substr($string, strlen('<front>'));
      }
      $uri = 'internal:' . $string;
    }

    return $uri;
  }

  /**
   * Form element validation handler for the 'uri' element.
   *
   * Disallows saving inaccessible or untrusted URLs.
   *
   * This method is almost the verbatim copy of
   * Drupal\link\Plugin\Field\FieldWidget\LinkWidget::validateUriElement.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $form
   *   The form structure.
   */
  public static function validateUriElement(array $element, FormStateInterface $form_state, array $form) {
    $uri = static::getUserEnteredStringAsUri($element['#value']);
    $form_state->setValueForElement($element, $uri);

    // If getUserEnteredStringAsUri() mapped the entered value to a 'internal:'
    // URI , ensure the raw value begins with '/', '?' or '#'.
    // @TODO '<front>' is valid input for BC reasons, may be removed by
    // https://www.drupal.org/node/2421941
    if (parse_url($uri, PHP_URL_SCHEME) === 'internal'
      && !in_array($element['#value'][0], ['/', '?', '#'], TRUE)
      && substr($element['#value'], 0, 7) !== '<front>') {
      $form_state->setError($element, new TranslatableMarkup('Manually entered paths should start with one of the following characters: / ? #'));
      return;
    }
  }

  /**
   * Get the available supports for the URL.
   *
   * @TODO: Find a way to avoid the LinkItemInterface dependency
   * because this is an interface for a field.
   *
   * @return array
   *   THe available supports.
   */
  protected static function getAvailableSupports() {
    return [
      LinkItemInterface::LINK_EXTERNAL,
      LinkItemInterface::LINK_INTERNAL,
      LinkItemInterface::LINK_GENERIC,
    ];
  }

  /**
   * Check if the user has access the uri.
   *
   * @param array $element
   *   The form element to process.
   *
   * @return bool
   *   TRUE if it has access. FALSE otherwise.
   */
  protected static function userHasAccessToUri(array $element) {
    $uri_isset = (isset($element['#default_value']['uri']) && !empty($element['#default_value']['uri']));

    $is_accessible = ($uri_isset && Url::fromUri($element['#default_value']['uri'])->access());

    return (\Drupal::currentUser()->hasPermission('link to any page') || $is_accessible);
  }

}
