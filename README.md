FORM ELEMENTS EXTRA
===================

 * Introduction
 * Requirements
 * Installation
 * Maintainers

INTRODUCTION
------------

Provides some additional form elements that we could regularly use.
For the moment, those additional form elements are :

 * `link_extra`: copy the link widget field behavior.

An issue has been created because we think some form elements needs
to be in core: https://www.drupal.org/node/2866997.


REQUIREMENTS
------------

This module requires no additional library.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.

MAINTAINERS
-----------

Current maintainers:
 * Benjamin Rambaud (beram) - https://drupal.org/user/3508624
